#ifndef Z0_CAESAR_H
#define Z0_CAESAR_H

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t  u8;
typedef uint32_t u32;

typedef struct {
	bool  valid;
	u8    shift;
	u32   len;
	u8   *data;
} z0_caesar;

void
z0_caesar_encrypt(z0_caesar*, u8*, u32);

void
z0_caesar_encrypt_shift(z0_caesar*, u8*, u32, u8);

void
z0_caesar_encrypt_valid(z0_caesar*, u8*, u32);

void
z0_caesar_encrypt_valid_shift(z0_caesar*, u8*, u32, u8);

void
z0_caesar_decrypt(z0_caesar*, u8*);

void
z0_caesar_valid(z0_caesar*);

void
z0_caesar_free(z0_caesar*);

void
z0_caesar_print(z0_caesar*);

#endif
