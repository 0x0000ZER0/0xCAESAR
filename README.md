# 0xCAESAR

A caesar cipher implementation in `C`.

### API

- Encrypting the given string. There are *4* functions for encryption. This particular function will default the shift to `3` and not check if the given string is valid or not. The `valid` boolean member will only be `false` when the malloc fails to allocate memory for the caller. 

```c
char txt[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
z0_caesar obj;
z0_caesar_encrypt(&obj, txt, sizeof (txt) - 1);

//...

z0_caesar_free(&obj);
```

- Encrypting the given string with given shift constant. This function will again not validate the given string:

```c
char txt[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
z0_caesar obj;
z0_caesar_encrypt_shift(&obj, txt, sizeof (txt) - 1, 7);

//...

z0_caesar_free(&obj);
```

- Encrypting the given string with validation. This method will validate and exncrypt the data. This proccess makes the validation slightly more efficent, because it checks and encrypts (if check is passed) at the same time. The `valid` member will be set to `false` when the string is NOT valid. The shift constant will default to `3`: 

```c
char txt[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
z0_caesar obj;
z0_caesar_encrypt_valid(&obj, txt, sizeof (txt) - 1);

if (obj.valid) {
    //...
}

z0_caesar_free(&obj);
```

- Encrypting the given string with given shift constant and additional validation. This function will again not validate the given string:

```c
char txt[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
z0_caesar obj;
z0_caesar_encrypt_valid_shift(&obj, txt, sizeof (txt) - 1, 5);

if (obj.valid) {
        //...
}

z0_caesar_free(&obj);
```

- Decrypting the given string. For this process you don't need to pass any extra data except the `z0_caesar` object. Note that the function will assume that the object is valid:

```c
z0_caesar obj = {
    .data  = "HIJKLMNOPQRSTUVWXYZABCDEFG",
    .shift = 7,
    .len   = 26,
    .valid = true
};

char seq[26];
z0_caesar_decrypt(&obj, seq);
```

- Validation the `z0_caesar` object. This will perform a full validation of the input data. Note that if you need to do the validation every time than this is going to be slower the the combined (encrypt + validation) functions shown above:

```c
z0_caesar obj = {
	.data  = "HIJKLMNOPQRSTUVWXYZABCDEFG",
	.len   = 26,
};

z0_caesar_valid(&obj);

if (obj.valid) {
        //...
}
```

- Printing the `z0_caesar` object. Note that by inluding the `valid` and `shift` fields into the struct does *NOT* waste any memory:

```c
z0_caesar_print(&obj);

// will print:
// Z0 caesar LEN:   {obj.len}   
// Z0 caesar SHIFT: {obj.shift}
// Z0 caesar DATA:  {obj.data}
// Z0 caesar VALID: {obj.valid}
```


