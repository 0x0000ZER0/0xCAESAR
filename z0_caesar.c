#include "z0_caesar.h"

#include <stdio.h>
#include <malloc.h>

void
z0_caesar_encrypt(z0_caesar *obj, u8 *seq, u32 len) {
	z0_caesar_encrypt_shift(obj, seq, len, 3);
}

void
z0_caesar_encrypt_shift(z0_caesar *obj, u8 *seq, u32 len, u8 sft) {
	obj->len   = len;
	obj->shift = sft;
	obj->data  = malloc(len * sizeof (u8) + 1);

	if (obj->data == NULL) {
		obj->valid = false;
		return;
	}

	obj->data[len] = '\0';
	obj->valid     = true;

	for (u32 i = 0; i < len; ++i)
		obj->data[i] = (((seq[i] - 'A') + obj->shift) % ('Z' - 'A' + 1)) + 'A';
}

void
z0_caesar_encrypt_valid(z0_caesar *obj, u8 *seq, u32 len) {
	z0_caesar_encrypt_valid_shift(obj, seq, len, 3);
}

void
z0_caesar_encrypt_valid_shift(z0_caesar *obj, u8 *seq, u32 len, u8 sft) {
	obj->len   = len;
	obj->shift = sft;
	obj->data  = malloc(len * sizeof (u8) + 1);

	if (obj->data == NULL) {
		obj->valid = false;
		return;
	}

	obj->data[len] = '\0';
	obj->valid     = true;
	
	for (u32 i = 0; i < len; ++i) {
		if (seq[i] >= 'A' && seq[i] <= 'Z') {
			obj->data[i] = (((seq[i] - 'A') + obj->shift) % ('Z' - 'A' + 1)) + 'A';
		} else {
			obj->valid   = false;
			obj->data[i] = '\0';
			obj->len     = i + 1;
			return;
		}
	}
}

void
z0_caesar_decrypt(z0_caesar *obj, u8 *seq) {
	for (u32 i = 0; i < obj->len; ++i) {
		seq[i] = obj->data[i] - obj->shift;
		if (seq[i] < 'A')
			seq[i] += ('Z' - 'A' + 1);
	}
}

void
z0_caesar_valid(z0_caesar *obj) {
	obj->valid = true;

	for (u32 i = 0; i < obj->len; ++i) {
		if (!(obj->data[i] >= 'A' && obj->data[i] <= 'Z')) {
			obj->valid = false;
			return;
		}
	}			
}

void
z0_caesar_free(z0_caesar *obj) {
	obj->len   = 0;
	obj->valid = false;

	free(obj->data);
}

void
z0_caesar_print(z0_caesar *obj) {
	printf("Z0 caesar LEN:   %u\n", obj->len);
	printf("Z0 caesar SHIFT: %u\n", obj->shift);
	printf("Z0 caesar DATA:  %.*s\n", obj->len, obj->data);
	printf("Z0 caesar VALID: %i\n", obj->valid);
}
